/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.ext.code.client;

import net.oschina.durcframework.easymybatis.util.ClassUtil;

public class ClientParam {
	private Class<?> mapperClass;
	private String templateClasspath; // 模板文件classpath路径

	public Class<?> getEntityClass() {
		return ClassUtil.getSuperInterfaceGenricType(mapperClass, 0);
	}
	
	public Class<?> getMapperClass() {
		return mapperClass;
	}

	public void setMapperClass(Class<?> mapperClass) {
		this.mapperClass = mapperClass;
	}

	public String getTemplateClasspath() {
		return templateClasspath;
	}

	public void setTemplateClasspath(String templateClasspath) {
		this.templateClasspath = templateClasspath;
	}
	
}
