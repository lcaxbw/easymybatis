/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.support;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import net.oschina.durcframework.easymybatis.HasPk;
import net.oschina.durcframework.easymybatis.PageResult;
import net.oschina.durcframework.easymybatis.dao.CrudDao;
import net.oschina.durcframework.easymybatis.query.Column;
import net.oschina.durcframework.easymybatis.query.Query;
import net.oschina.durcframework.easymybatis.query.Queryable;
import net.oschina.durcframework.easymybatis.query.expression.Expressional;
import net.oschina.durcframework.easymybatis.support.lock.DefaultRecordLock;
import net.oschina.durcframework.easymybatis.support.lock.RecordLock;
import net.oschina.durcframework.easymybatis.util.MyBeanUtil;
import net.oschina.durcframework.easymybatis.util.QueryUtils;

/**
 * 通用service
 * @author tanghc
 *
 * @param <Dao>
 * @param <Entity>
 */
public class CrudService<Dao extends CrudDao<Entity>,Entity extends HasPk<?>> implements CrudDao<Entity> {
	
	private Dao dao;
	
	private static RecordLock DEFAULT_RECORD_LOCK = new DefaultRecordLock();
	
	public Dao getDao() {
		return dao;
	}

	@Autowired
	public void setDao(Dao dao) {
		this.dao = dao;
	}
	
	protected RecordLock getRecordLock() {
		return DEFAULT_RECORD_LOCK;
	}

	public PageResult<Entity> query(Query query) {
		return QueryUtils.query(dao, query);
	}
	
	public <T extends PageResult<Entity>> T query(Query query,Class<T> resultClass) {
		return QueryUtils.query(dao, query, resultClass);
	}

	@Override
	public int update(Entity entity) {
		return this.doLockUpdate(new Updater<Entity>() {
			@Override
			public int doLockUpdate(Entity entity) {
				return getDao().update(entity);
			}
		}, entity);
	}

	@Override
	public int updateIgnoreNull(Entity entity) {
		return this.doLockUpdate(new Updater<Entity>() {
			@Override
			public int doLockUpdate(Entity entity) {
				return getDao().updateIgnoreNull(entity);
			}
		}, entity);
	}
	
	@Override
	public int updateIgnoreNullByExpression(Entity entity, final Expressional expressional) {
		return this.doLockUpdate(new Updater<Entity>() {
			@Override
			public int doLockUpdate(Entity entity) {
				return getDao().updateIgnoreNullByExpression(entity, expressional);
			}
		}, entity);
	}
	
	private int doLockUpdate(Updater<Entity> updater,Entity entity) {
		// 单机部署情况下可以这样做
		// 集群的话要考虑分布式锁
		RecordLock recordLock = this.getRecordLock();
		
		recordLock.lock(entity);
		
		Entity obj = this.get(entity);
		MyBeanUtil.copyProperties(entity, obj);
		
		int i = updater.doLockUpdate(obj);
		
		recordLock.unlock(entity);
		
		return i;
	}
	
	static interface Updater<Entity> {
		int doLockUpdate(Entity entity);
	}

	@Override
	public Entity get(Object id) {
		return this.getDao().get(id);
	}

	@Override
	public Entity getByExpression(Queryable Queryable) {
		return this.getDao().getByExpression(Queryable);
	}

	@Override
	public Entity getByProperty(String column, Object value) {
		return this.getDao().getByProperty(column, value);
	}

	@Override
	public List<Entity> listByProperty(String column, Object value) {
		return this.getDao().listByProperty(column, value);
	}

	@Override
	public List<Entity> listByProperty(String column, Object value, Queryable Queryable) {
		return this.getDao().listByProperty(column, value, Queryable);
	}

	@Override
	public List<Entity> find(Queryable Queryable) {
		return this.getDao().find(Queryable);
	}

	@Override
	public long countTotal(Queryable Queryable) {
		return this.getDao().countTotal(Queryable);
	}

	@Override
	public List<Map<String, Object>> findProjection(Queryable Queryable) {
		return this.getDao().findProjection(Queryable);
	}

	@Override
	public int save(Entity entity) {
		return this.getDao().save(entity);
	}

	@Override
	public int saveIgnoreNull(Entity entity) {
		return this.getDao().saveIgnoreNull(entity);
	}

	@Override
	public int saveBatch(List<Entity> entitys) {
		return this.getDao().saveBatch(entitys);
	}

	@Override
	public int saveBatchWithColumns(List<Column> columns, List<Entity> entitys) {
		return this.getDao().saveBatchWithColumns(columns, entitys);
	}

	@Override
	public int saveMulti(List<Entity> entitys) {
		return this.getDao().saveMulti(entitys);
	}

	@Override
	public int saveMultiWithColumns(List<Column> columns, List<Entity> entitys) {
		return this.getDao().saveMultiWithColumns(columns, entitys);
	}

	@Override
	public int del(Entity entity) {
		return this.getDao().del(entity);
	}

	@Override
	public int delByExpression(Queryable query) {
		return this.getDao().delByExpression(query);
	}

}
